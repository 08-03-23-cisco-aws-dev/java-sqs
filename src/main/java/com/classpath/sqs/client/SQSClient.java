package com.classpath.sqs.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageRequest;

public class SQSClient {
	
	// credentials
	private static final AWSCredentials CREDENTIALS;
	
	static {
		CREDENTIALS = new BasicAWSCredentials("AKIA4DNDJF4CH54YAPWC", "9Rjcs7dkdekIeAnlHJCWwQKFzlUXfqVleh+9B+T2");
	}
	
	public static void main(String[] args) throws InterruptedException {
		
		AmazonSQS amazonSQSClient = AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(CREDENTIALS))
				.withRegion(Regions.AP_SOUTH_1)
				.build();
		
		//createSQSQueue(amazonSQSClient);
		//sendMessage(amazonSQSClient);
		//sendMessagesInBatch(amazonSQSClient);
		readMessages(amazonSQSClient);
		Thread.sleep(40000);
		
	}

	private static void readMessages(AmazonSQS amazonSQSClient) {
		 ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest("https://sqs.ap-south-1.amazonaws.com/831955480324/messages-queue").withWaitTimeSeconds(10) // Long polling;
		            .withMaxNumberOfMessages(1); // Max is 10
		 List<Message> sqsMessages = amazonSQSClient.receiveMessage(receiveMessageRequest)
		            .getMessages();

		        sqsMessages.get(0)
		            .getAttributes();
		        sqsMessages.get(0)
		            .getBody();
		
	}

	private static void sendMessagesInBatch(AmazonSQS amazonSQSClient) {
		 List<SendMessageBatchRequestEntry> messageEntries = new ArrayList<>();
	        messageEntries.add(new SendMessageBatchRequestEntry().withId("id-1")
	            .withMessageBody("batch-1")
	            .withMessageGroupId("app-1"));
	        messageEntries.add(new SendMessageBatchRequestEntry().withId("id-2")
	            .withMessageBody("batch-2")
	            .withMessageGroupId("app-1"));

	        SendMessageBatchRequest sendMessageBatchRequest = new SendMessageBatchRequest("https://sqs.ap-south-1.amazonaws.com/831955480324/messages-queue", messageEntries);
	        amazonSQSClient.sendMessageBatch(sendMessageBatchRequest);
		
	}

	private static void sendMessage(AmazonSQS amazonSQSClient) {
		Map<String, MessageAttributeValue> map = new HashMap<>();
		map.put("location", new MessageAttributeValue().withStringValue("Bangalore").withDataType("String"));
		
		 SendMessageRequest sendMessageStandardQueue = new SendMessageRequest().withQueueUrl("https://sqs.ap-south-1.amazonaws.com/831955480324/messages-queue")
		            .withMessageBody("Another simple message.")
		            .withDelaySeconds(5)
		            .withMessageAttributes(map);
		 
		 amazonSQSClient.sendMessage(sendMessageStandardQueue);
		
	}

	private static void createSQSQueue(AmazonSQS amazonSQSClient) {
		CreateQueueRequest createQueueRequest = new CreateQueueRequest("messages-queue");
		CreateQueueResult result = amazonSQSClient.createQueue(createQueueRequest);
		System.out.println("The Queue URL is :: "+result.getQueueUrl());
		
	}

}
